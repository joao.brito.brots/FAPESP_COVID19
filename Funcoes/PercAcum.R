#' Percentual e acumulado
#'
#' @return
#' @export
#'
#' @examples
PercAcum <- function(dados){
  
  dados %<>% 
    mutate(
      Perc = (n / sum(n) * 100L),
      PercAcum = cumsum(Perc)
    )
  
  # Retorno da funcao
  return(dados)
}
####
## Fim
#