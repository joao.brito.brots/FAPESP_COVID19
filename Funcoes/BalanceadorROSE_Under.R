#' Balanceamento de dados por classe ROSE Under
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
BalanceadorROSE_Under <- function(dados){

  # Faz balanceamento dos dados
  dados <<- dados
  dadosBalanceados <- ovun.sample(
    formula = FoiParaUTI ~ ., 
    data = dados,
    method = "under",
    seed = 1L
  )$data %>% 
    as_tibble() 
  
  # Retorno da funcao
  return(dadosBalanceados) 
}
####
## Fim
#