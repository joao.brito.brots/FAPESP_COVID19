#' Selecao de variaveis Forward 
#'
#' @param dados 
#' @param balanceador 
#'
#' @return
#' @export
#'
#' @examples
SeletorVariaveisForward <- function(dados){
  
  # Gera ranking de variaveis
  rankingVariaveis <- ChamarFuncao(
    dados = dados,
    funcao = parSys$SeletorVariaveis$Ranking
  )
  
  # Inicializa selecionadas
  varSelecionadas <- character()
  
  # Resultados a cada adicao de variavel
  resultadosIteracao <- tibble()

  # Processo iterativo de adicao de variaveis
  for(varCandidata in rankingVariaveis$Variavel){
    
    # Exibe processamento
    Msg(
      bold(green("Selecao variaveis Forward...")),
      green("\nCandidata: "), green(varCandidata)
    )
    
    # Adiciona candidata
    varSelecionadas <- c(varSelecionadas, varCandidata)
    
    # Cria modelo preditivo
    modelo <- ChamarFuncao(
      dados = dados %>% 
        select(FoiParaUTI, !!varSelecionadas),
      funcao = parSys$SeletorVariaveis$Classificador
    )
    
    # Se houve erro 
    if(Erro(modelo)){
      next()
    }
    
    # Matriz de confusao
    matrizConfusao <- VerificarAcuracia(
      dadosTeste = dados %>% 
        select(FoiParaUTI, !!varSelecionadas),
      modelo = modelo,
      classificador = parSys$SeletorVariaveis$Classificador
    )
    
    # Se houve erro 
    if(Erro(matrizConfusao)){
      next()
    }
    
    # Resultados de cada iteracao
    resultadosIteracao %<>% 
      bind_rows(
        tibble(
          Variavel = varCandidata,
          CRV = matrizConfusao$CRV
        )
      )
    
    # Se finalizou
    if(matrizConfusao$CRV == 1){
      # Finaliza processo
      break()
    }
  }
  
  # Adiciona id
  resultadosIteracao %<>% 
    mutate(
      Linha = row_number()
    )
  
  # Variaveis selecionada
  varSelecionadas <- resultadosIteracao %>% 
    filter(
      Linha <= (
        resultadosIteracao %>% 
          mutate(
            CRV = round(CRV, 3)
          ) %>% 
          filter(
            (CRV == max(CRV))
          ) %>% 
          slice(1) %>% 
          pull(Linha)
      )
    ) %>% 
    pull(Variavel)
  
  # Retorno da funcao
  return(varSelecionadas)
}
####
## Fim
#