#' Dados COVID Albert Einstein
#' 
#' Dados anonimizados coletados de pacientes que fizeram 
#' teste para o COVID-19 (sorologia ou PCR), e respectivos 
#' resultados de exames laboratoriais.
#'
#' Fonte: https://repositoriodatasharingfapesp.uspdigital.usp.br/handle/item/83
#'
#' @return Dados COVID Albert Einstein
#' @export
#'
#' @examples
CarregarDadosOriginais_AlbertEinstein <- function(){
  
  # Carrega dados pacientes
  pacientes <- ReadDados(
    arquivo = "Dados/einstein_small_dataset_paciente1.csv"
  ) %>% 
    rename_all(TratarAlfanumericos) %>% 
    mutate_all(TratarAlfanumericos) %>%
    mutate(
      ORIGEM = "AlbertEinstein"
    ) %>% 
    select(ORIGEM, everything())
  
  # Carrega dados exames
  exames <- ReadDados(
    arquivo = "Dados/einstein_small_dataset_exames.csv"
  ) %>% 
    mutate(
      dt_coleta  = dt_coleta  %>% 
        dmy()
    ) %>% 
    rename_all(TratarAlfanumericos) %>% 
    mutate_all(TratarAlfanumericos) %>%
    mutate(
      ORIGEM = "AlbertEinstein"
    ) %>% 
    select(ORIGEM, everything())
  
  # Dados
  dados <- list(
    Pacientes = pacientes,
    Exames = exames
  )
  
  # Retorno da funcao
  return(dados)
}
####
## Fim
#