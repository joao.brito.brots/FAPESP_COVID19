#' Gera ranking de variaveis - Relief 
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
RankingRelief <- function(dados){
  
  # Apura ranking
  rankingVariaveis <- relief(
    formula = FoiParaUTI ~.,
    data = dados
  ) %>% 
    rownames_to_column(
      var = "Variavel"
    ) %>% 
    as_tibble() %>% 
    rename(
      Score = attr_importance
    ) %>% 
    arrange(desc(Score))
  
  # Retorno da funcao
  return(rankingVariaveis)
}
####
## Fim
#