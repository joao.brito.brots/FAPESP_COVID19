#' Balanceamento de dados por classe MWMOTE
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
BalanceadorMWMOTE <- function(dados){
  
  # Faz balanceamento dos dados
  dadosBalanceados <- oversample(
    dataset = dados %>% 
      mutate_all(LogicalToNumericLogical) %>% 
      mutate(
        FoiParaUTI = FoiParaUTI %>% 
          as.character() %>% 
          as.numeric()
      ) %>% 
      as.data.frame(),
    method = "MWMOTE",
    classAttr = "FoiParaUTI",
    ratio = parSys$Balanceador$Ratio
  ) %>% 
    as_tibble() %>% 
    mutate_all(NumericLogicalToLogical) %>% 
    mutate(
      FoiParaUTI = FoiParaUTI %>% 
        as.logical() %>% 
        LogicalToFactorLogical()
    ) 
  
  # Retorno da funcao
  return(dadosBalanceados) 
}
####
## Fim
#