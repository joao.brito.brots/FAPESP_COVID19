#' Cria modelo KNN
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
ClassificadorKNN <- function(dados){
  
  # Cria modelo preditivo
  modelo <- knn3(
    formula = FoiParaUTI ~ .,
    data = dados,
    k = 5
  )
  
  # Retorno da funcao
  return(modelo) 
}
####
## Fim
#