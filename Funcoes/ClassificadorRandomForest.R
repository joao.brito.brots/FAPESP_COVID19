#' Gera modelo Random Forest
#'
#' @param dados 
#'
#' @return
#' @export
#'
#' @examples
ClassificadorRandomForest <- function(dadosTreino){
  
  # Cria modelo
  modelo <- randomForest(
    formula = FoiParaUTI ~ .,
    data = dadosTreino
  )
  
  # Retorno da funcao
  return(modelo)
}
####
## Fim
#