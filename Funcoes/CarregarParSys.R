#' Carrega parametros do sistema
#'
#' @return
#' @export
#'
#' @examples
CarregarParSys <- function(){
  
  # Carga inicial dos parametros no Config.ini
  parSys <- CarregarConfig.ini()
  
  # Funcoes Scale dados
  if(parSys$Scale$Funcoes == "Todos"){
    parSys$Scale$Funcoes <- dir(
      path = "Funcoes",
      pattern = "^Scale.+\\.R$"
    ) %>% 
      str_remove_all("\\.R$") %>% 
      append("Nenhum")
  }
  
  # Funcoes Balanceamento dados
  if(parSys$Balanceador$Funcoes == "Todos"){
    parSys$Balanceador$Funcoes <- dir(
      path = "Funcoes",
      pattern = "^Balanceador.+\\.R$"
    ) %>% 
      str_remove_all("\\.R$") 
  }
  
  # Assume vetor de metodos disponiveis
  if(parSys$Classificador$Funcoes == "Todos"){
    parSys$Classificador$Funcoes <- dir(
      path = "Funcoes",
      pattern = "^Classificador.+\\.R$"
    ) %>% 
      str_remove_all("\\.R$")
  }
  
  # Retorno da funcao
  return(parSys)
}
####
## Fim
#